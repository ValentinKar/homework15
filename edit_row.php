<?php 
error_reporting(E_ALL);        // вывести на экран все ошибки
require_once('function.php');   // соединение с базой данных
if ( isEditRow($_GET['row']) )  {  header('Location: index.php');  }
?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>edit row</title>
</head>
<body>

<h1>Редактировать поле  
<?php  echo $_GET['row'];  ?> 
  в табличке  
<?php  echo $_GET['table'];  ?> 
  в базе данных  
<?php  echo $_GET['db'];  ?>
</h1>

    <form method="POST">
        <input type="hidden" name="name_db" value="<?php echo $_GET['db']; ?>" />
        <input type="hidden" name="name_table" value="<?php echo $_GET['table']; ?>" />
    	<input type="text" name="name_row" placeholder="название поля" value="<?php echo $_GET['row']; ?>" />
        <input type="text" name="type_row" placeholder="тип поля" value="<?php echo $_GET['type']; ?>" />
        <input type="submit" name="edit" value="Редактировать поле" />
    </form>

</body>
</html>