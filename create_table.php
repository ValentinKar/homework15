<?php 
error_reporting(E_ALL);        // вывести на экран все ошибки
require_once('function.php');   // соединение с базой данных
if ( isCreateTable() )  {  header('Location: index.php');  }
?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>create table</title>
</head>
<body>

<h1>Создать табличку в базе данных 
<?php  echo DB_NAME;  ?> 
</h1>

    <form method="POST">
        <input type="text" name="name_table" placeholder="название таблички" value="students" />
        <input type="submit" name="create" value="Создать табличку" />
    </form>

</body>
</html>