<?php 
error_reporting(E_ALL);        // вывести на экран все ошибки
require_once('function.php');   // соединение с базой данных
if ( isDeleteRow($_GET['row']) )  {  header('Location: index.php');  }
?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>delete row</title>
</head>
<body>

<h2>Удалить поле  
<?php  echo $_GET['row'];  ?> 
  в табличке  
<?php  echo $_GET['table'];  ?> 
  в базе данных  
<?php  echo $_GET['db'];  ?> 
?
</h2>

    <form method="POST">
        <input type="hidden" name="name_db" value="<?php echo $_GET['db']; ?>" />
        <input type="hidden" name="name_table" value="<?php echo $_GET['table']; ?>" />
        <input type="hidden" name="name_row" value="<?php echo $_GET['row']; ?>" />
		<?php  
		foreach ( requestDB("SELECT ".$_GET['row']." FROM ", $_GET['table'].";") as $rows ) :  
			echo htmlspecialchars( $rows[0] ) . '<br />'; 
		endforeach;  
		?> 
		<br />
        <input type="submit" name="edit" value="Удалить поле" />
    </form>

</body>
</html>