<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
require_once('connect_DB.php');   // соединение с базой данных


function requestDB($request, $address) 
{ 
    $data_bases = Connect( getDB('db', DB_NAME) )->prepare($request . $address); 
    $data_bases->execute();  
return $data_bases; 
} 

function getDB($request, $answer) 
{ 
    $_GET[$request] = isset($_GET[$request]) ? $_GET[$request] : $answer;
    $ans = $_GET[$request]; 
return $ans; 
} 

function isEditRow($row) 
{ 
if ( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['name_db']) && isset($_POST['name_table']) && isset($_POST['name_row']) && isset($_POST['type_row']) )  { 
		$statement = Connect($_POST['name_db'])->prepare("ALTER TABLE ".$_POST['name_table']." CHANGE $row ".$_POST['name_row']." ".$_POST['type_row'].";"); 
		$statement->execute(); 
	return true; 
}; 
return false;
} 

function isDeleteRow($row) 
{ 
if ( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['name_db']) && isset($_POST['name_table']) && isset($_POST['name_row']) )  { 
	$statement = Connect( $_POST['name_db'] )->prepare("ALTER TABLE ".$_POST['name_table']." DROP COLUMN ".$_POST['name_row'].";"); 
	$statement->execute(); 
	return true; 
}; 
return false;
}

function isCreateTable() 
{ 
if ( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['name_table']) )  { 
	$statement = Connect(DB_NAME)->prepare(" 
	CREATE TABLE ".(string)$_POST['name_table']." (
	`id` INT(11) NOT NULL AUTO_INCREMENT, 
	`name` VARCHAR(50) NOT NULL, 
	`estimation` FLOAT NOT NULL, 
	`budget` TINYINT(4) NOT NULL DEFAULT '0', 
	PRIMARY KEY (id) 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 
	;"); 
	$statement->execute(); 
	return true; 
}; 
return false;
}