<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
include_once('function.php'); 
?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>data base</title>
<style>
table {
padding: 4px;
}
td {
padding: 4px;
}
</style>
</head>
<body>

    <a href='create_table.php'>
    Создать табличку в базе данных 
    <?php echo DB_NAME; ?>
    </a> 

<h1>Список баз данных: </h1>

<ul>
    <?php 
    foreach ( requestDB("SHOW DATABASES;", '')  as $data_base )  :   
    ?>
        <li>
            <a href='?db=<?php  echo $data_base[0];  ?>'>
                <?php  echo $data_base[0];  ?>
            </a> 
        </li>
    <?php  
    endforeach; 
    ?>
</ul>

<h2>Список таблиц базы данных  
<?php  echo getDB('db', DB_NAME);  ?>
</h2>
<ul>
    <?php  foreach ( requestDB("SHOW TABLES;", '') as $table )  :  ?>
    <li>
        <a href='?db=<?php echo getDB('db', DB_NAME); ?>&table=<?php echo $table[0]; ?>'>
            <?php  echo $table[0];  ?>
        </a> 
    </li>
    <?php  endforeach;  ?>
</ul>

<?php  
$table = getDB('table', false); 
if ( $table )  :  
?> 
<h2>Список полей таблицы   
<?php  echo getDB('table', '');  ?>
</h2>
<table>
    <?php  foreach ( requestDB("DESCRIBE ", $table.';' ) as $rows)  :  ?>
    <tr>
        <td>название поля: </td>
        <td><?php echo $rows[0]; ?></td>
        <td> <a href='edit_row.php?db=<?php echo getDB('db',''); ?>&table=<?php echo $table; ?>&row=<?php echo $rows[0]; ?>&type=<?php echo $rows[1]; ?>'> Переименовать или изменить тип поля </a> </td>
        <td> <a href='delete_row.php?db=<?php echo getDB('db',''); ?>&table=<?php echo $table; ?>&row=<?php echo $rows[0]; ?>'> Удалить поле </a> </td>
    </tr>
    <tr>
        <td>тип поля: </td>
        <td><?php echo $rows[1]; ?></td>
    </tr>
    <tr>
        <td>может ли содержать NULL: &nbsp;&nbsp;</td>
        <td><?php echo $rows[2]; ?></td>
    </tr>
    <tr>
        <td>PRI - первичный ключ: </td>
        <td><?php echo $rows[3]; ?></td>
    </tr>
    <tr>
        <td>по умолчанию: </td>
        <td><?php echo $rows[4]; ?></td>
    </tr>
    <tr>
        <td>авто нарастание: </td>
        <td><?php echo $rows[5]; ?></td>
    </tr>
    <tr>
        <td> ------ </td>
    </tr>
    <?php  endforeach;  ?>
</table>
<?php  endif;  ?>

</body>
</html>